#include "main.h"
#include <stdint.h>
#include <math.h>

_Noreturn void eth_thread (__attribute__((unused)) const void *obj) {
    uint8_t phy_addr = 0;

    SET_ETH_CLK();

    if (!phy_init(&phy_addr)) {
        mc_reset();
    }

    while (!phy_is_link(phy_addr)) {
        vsrtos_delay(MS_TO_TICK(100));
    }

    SET_ETH_MAC(0x00, 0x08, 0xDC, 0x71, 0x72, 0x77);
    SET_ETH_IP(192, 168, 1, 69);
    SET_ETH_GW(192, 168, 1, 1);
    SET_ETH_MSK(255, 255, 255, 0);

    socket_udp_close(ETH_SOCKET_0);
    socket_udp_open(ETH_SOCKET_0, UDP_RX_PORT);

    huP7TelId sin_val = uP7_TELEMETRY_INVALID_ID;
    if (!uP7TelCreateCounter("sin_test", -10000, -10000, 10000, 10000, true, &sin_val)) {
        while (true);
    }

    uint32_t last_time = vsrtos_get_time();
    while (true) {
        for (int32_t i = -10000; i <= 10000; i++) {
            uP7TelSentSample(sin_val, i);
            vsrtos_delay_until(&last_time, MS_TO_TICK(1));
        }
    }
}