#include "main.h"
#include "uP7preprocessed.h"

uint64_t get_timer_frequency (void *i_pCtx) {
    UNUSED_ARG(i_pCtx);
    return OS_TICK_FREQ;
}

uint64_t get_timer_value (void *i_pCtx) {
    UNUSED_ARG(i_pCtx);
    return vsrtos_get_time();
}

bool send_telem_packet (void *i_pCtx, enum euP7packetRank i_eRank,
                        const struct stP7packetChunk *i_pChunks,
                        size_t i_szChunks, size_t i_szData) {
    UNUSED_ARG(i_pCtx);
    UNUSED_ARG(i_eRank);
    UNUSED_ARG(i_szData);
    while (i_szChunks--) {
        socket_udp_send(ETH_SOCKET_0, ETH_SOCKET_0_TX, i_pChunks->pData, i_pChunks->szData,
                        SET_SOCKET_TX_IP(192, 168, 1, 22), UDP_TX_PORT);
        i_pChunks++;
    }

    return true;
}

static mutex_t m;
void p7_lock (void *i_pCtx) {
    UNUSED_ARG(i_pCtx);
    vsrtos_mutex_lock(&m, ALWAYS_WAIT);
}

void p7_unlock(void *i_pCtx) {
    UNUSED_ARG(i_pCtx);
    vsrtos_mutex_unlock(&m);
}

void init_up7 () {
    struct stuP7config l_stConfig;

    l_stConfig.bSessionIdCrc7 = g_bCrc7;
    l_stConfig.uSessionId = g_uSessionId;
    l_stConfig.pCtxTimer = NULL;
    l_stConfig.cbTimerFrequency = get_timer_frequency;
    l_stConfig.cbTimerValue = get_timer_value;
    l_stConfig.pCtxLock = NULL;
    l_stConfig.cbLock = p7_lock;
    l_stConfig.cbUnLock = p7_unlock;
    l_stConfig.pCtxPacket = NULL;
    l_stConfig.cbSendPacket = send_telem_packet;
    l_stConfig.pCtxThread = NULL;
    l_stConfig.cbGetCurrentThreadId = NULL;
    l_stConfig.eDefaultVerbosity = euP7Level_Trace;

    l_stConfig.pModules = g_pModules;
    l_stConfig.szModules = g_szModules;
    l_stConfig.pTraces = g_pTraces;
    l_stConfig.szTraces = g_szTraces;
    l_stConfig.pTelemetry = g_pTelemetry;
    l_stConfig.szTelemetry = g_szTelemetry;
    l_stConfig.eAlignment = euP7alignment4;

    uP7initialize(&l_stConfig);
}
