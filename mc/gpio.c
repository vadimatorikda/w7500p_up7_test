#include "mc.h"

#define GPIO_LED     GPIOC
#define AFC_LED      AFC_C
#define PADCON_LED   PADCON_PC
#define ALT_FUNC_LED 1
#define PIN_LED      0

void init_led_pin () {
    set_gpio_out(GPIO_LED, PIN_LED);
    gpio_set(GPIO_LED, PIN_LED);
    set_gpio_alt_func(AFC_LED, PIN_LED, ALT_FUNC_LED);
    set_gpio_open_drain(PADCON_LED, PIN_LED);
}

void led_set_state (uint8_t state) {
    if (state) {
        gpio_reset(GPIO_LED, PIN_LED);
    } else {
        gpio_set(GPIO_LED, PIN_LED);
    }
}
